<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
//user middleware
Route::group(['middleware'=>'admin'],function () {
    Route::get('/stage1','AdminController@stage1');
    Route::get('/stage2','AdminController@stage2');
    Route::get('/stage3','AdminController@stage3');
    Route::get('/accept/{id}','AdminController@accept');
    Route::get('/reject/{id}','AdminController@reject');
    Route::get('/admin','AdminController@index');
    Route::get('/all_proposals','AdminController@all_proposals');
    Route::get('/preview/{id}', 'AdminController@preview');
});
Route::group(['middleware'=>'applicants'],function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/proposal','ApplicantsController@index');

}
);
Route::get('/print/{id}','AdminController@printit');
Route::get('/fetchUnread','AdminController@fetchUnread');



//user activation link verification
Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');
