@extends('admin')
@section('content')
    @if(count($proposals)>0)
    <table class="table table-stripped">
        <thead>
        <th>
            Proposal Title
        </th>
        <th>
            Organisation
        </th>
        <th>
            Submitted by
        </th>
        <th>
            Email
        </th>
        <th>
            Date
        </th>
        <th>
            Action
        </th>
        </thead>
        @foreach($proposals as $proposal)
            <tbody>
            <tr>
                <td>
                    {{$proposal->title}}
                </td>
                <td>
                    {{$proposal->organisation_name}}
                </td>
                <td>
                    {{$proposal->submitted_by}}
                </td>
                <td>
                    {{$proposal->email}}
                </td>
                <td>
                    {{$proposal->created_at}}
                </td>
                <td>
                    <a href='{{url("/preview/{$proposal->id}")}}' class="btn btn-success">Preview proposal</a>
                </td>
            </tr>
            </tbody>
            @endforeach

    </table>
    @else
    <p class="text-center">No proposals posted yet</p>
    @endif
    @endsection