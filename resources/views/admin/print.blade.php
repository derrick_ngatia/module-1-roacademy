<table class="table table-bordered">
    <thead>
    <th class="text-center" colspan="2">
        {{$proposal->title}}
    </th>
    </thead>
    <tbody>
    <tr>
        <td>

        </td>
        <td>

        </td>
    </tr>
    <tr>
        <td>
            Organisation Name
        </td>
        <td>
            {{$proposal->organisation_name}}
        </td>
    </tr>
    <tr>
        <td>
            Address
        </td>
        <td>
            {{$proposal->address}}
        </td>
    </tr>
    <tr>
        <td>
            Phone
        </td>
        <td>
            {{$proposal->phone}}
        </td>
    </tr>
    <tr>
        <td>
            Email
        </td>
        <td>
            {{$proposal->email}}
        </td>
    </tr>
    <tr>
        <td>
            Submitted By
        </td>
        <td>
            {{$proposal->submitted_by}}
        </td>
    </tr>
    <tr>
        <td>
            Summary
        </td>
        <td>
            {{$proposal->summary}}
        </td>
    </tr>
    <tr>
        <td>
            Activities
        </td>
        <td>
            {{$proposal->activities}}
        </td>
    </tr>
    <tr>
        <td>
            Background
        </td>
        <td>
            {{$proposal->background}}
        </td>
    </tr>
    <tr>
        <td>
            Budget
        </td>
        <td>
            {{$proposal->budget}}
        </td>
    </tr>
    </tbody>
</table>