@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center">{{Auth::user()->name}} Submit your Proposal</div>
                @include('flash::message')
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action='{{url("/proposal")}}'>
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="title" class="col-md-4 control-label">Proposal Title</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" required autofocus>

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <h4 class="text-center">Organisation Details</h4>
                        <hr>
                        <div class="form-group{{ $errors->has('organisation_name') ? ' has-error' : '' }}">
                            <label for="organisation_name" class="col-md-4 control-label">Organization Name</label>

                            <div class="col-md-6">
                                <input id="organisation_name" type="text" class="form-control" name="organisation_name" value="{{ old('organisation_name') }}" required autofocus>

                                @if ($errors->has('organisation_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('organisation_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Address</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address" value="{{ old('address') }}" required autofocus>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <h4 class="text-center">User Details</h4>
                        <hr>
                        <div class="form-group{{ $errors->has('submitted_by') ? ' has-error' : '' }}">
                            <label for="submitted_by" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="submitted_by" type="text" class="form-control" name="submitted_by" value="{{ old('submitted_by') }}" required autofocus>

                                @if ($errors->has('submitted_by'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('submitted_by') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('submitted_title') ? ' has-error' : '' }}">
                            <label for="submitted_title" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                <input id="submitted_title" type="text" class="form-control" name="submitted_title" value="{{ old('submitted_title') }}" required autofocus>

                                @if ($errors->has('submitted_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('submitted_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('summary') ? ' has-error' : '' }}">
                            <label for="summary" class="col-md-4 control-label">Summary</label>

                            <div class="col-md-6">
                                <textarea id="summary"  class="form-control"
                                          name="summary"required autofocus>{{ old('summary') }}</textarea>

                                @if ($errors->has('summary'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('summary') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('background') ? ' has-error' : '' }}">
                            <label for="background" class="col-md-4 control-label">Background</label>

                            <div class="col-md-6">
                                <textarea id="background"  class="form-control" name="background" required autofocus>{{ old('background') }}</textarea>

                                @if ($errors->has('background'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('background') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('activities') ? ' has-error' : '' }}">
                            <label for="activities" class="col-md-4 control-label">Activities</label>

                            <div class="col-md-6">
                                <textarea id="activities" class="form-control"  name="activities"required autofocus>{{ old('activities') }}</textarea>

                                @if ($errors->has('activities'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('activities') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('budget') ? ' has-error' : '' }}">
                            <label for="budget" class="col-md-4 control-label">Budget</label>

                            <div class="col-md-6">
                                <input type="number" id="budget"  class="form-control"  name="budget" required autofocus/>

                                @if ($errors->has('budget'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('budget') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">

                                <button type="submit" id="submit" name="submit" value="draft" class="btn btn-warning">
                                    Save as Draft
                                </button>
                                <button type="submit" id="submit" name="submit" value="submit" class="btn btn-success">
                                    Submit Proposal
                                </button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
