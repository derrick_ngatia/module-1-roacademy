@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">Await our response</div>
                    @include('flash::message')
                    <div class="panel-body">
                        <p class="text-justify" style="font-weight: bold">We are still processing your application for a proposal review. We aim to let you know the status of your application as soon as possible.  </p>
                        <button class="btn btn-danger">Logout</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection