<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     *
     */
    protected $redirectTo = '/';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
   protected function authenticated(Request $request, $user)
   {
       if (!$user->verified) {
           auth()->logout();
           flash('You need to confirm your account. We have sent you an activation code, please check your email.')->error();
           return redirect()->intended('/');
       }else{
           if(($user->is_admin===true)){
               return redirect()->intended('admin');

           }
           if(($user->is_admin===false)){
               return redirect()->intended('home');

           }
       }
   }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

}
