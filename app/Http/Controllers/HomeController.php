<?php

namespace App\Http\Controllers;

use App\Proposal;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $proposal=Proposal::where('user_id',Auth::user()->id)->exists();
        if($proposal){
            $proposals=Proposal::where('user_id',Auth::user()->id)->first();
            if ($proposals->is_Submit){
                return view('submited');
            }else{
                return view('draft',compact('proposals'));
            }


        }
        else{
            return view('home');
        }
    }
}
