<?php

namespace App\Http\Controllers;

use App\Proposal;
use Auth;
use Illuminate\Http\Request;

class ApplicantsController extends Controller
{
    //


    public function index(Request $request){
        $this->validate($request,['title'=>'required','organisation_name'=>
            'required','address'=>'required'
            ,'phone'=>'required|max:10|min:10','email'=>'required',
            'submitted_by'=>'required','summary'=>'required','background'=>'required','activities'=>'required','budget'=>'required|integer']);
        if($request->input('submit')=='draft'){
            $proposal=new Proposal();
            $proposal->user_id=Auth::id();
            $proposal->title=$request->input('title');
            $proposal->organisation_name=$request->input('organisation_name');
            $proposal->address=$request->input('address');
            $proposal->phone=$request->input('phone');
            $proposal->email=$request->input('email');
            $proposal->submitted_by=$request->input('submitted_by');
            $proposal->submitted_title=$request->input('submitted_title');
            $proposal->summary=$request->input('summary');
            $proposal->background=$request->input('background');
            $proposal->activities=$request->input('activities');
            $proposal->budget=$request->input('budget');
            $proposal->save();
            flash('Proposal saved as draft')->success();
            return redirect()->back();
        }else{
            $proposal=new Proposal();
            $proposal->user_id=Auth::user()->id;
            $proposal->title=$request->input('title');
            $proposal->organisation_name=$request->input('organisation_name');
            $proposal->address=$request->input('address');
            $proposal->phone=$request->input('phone');
            $proposal->email=$request->input('email');
            $proposal->submitted_by=$request->input('submitted_by');
            $proposal->submitted_title=$request->input('submitted_title');
            $proposal->summary=$request->input('summary');
            $proposal->background=$request->input('background');
            $proposal->activities=$request->input('activities');
            $proposal->budget=$request->input('budget');
            $proposal->is_submit=true;
            $proposal->save();
            flash('Proposal Sent')->success();
            return redirect()->back();


        }


    }
}
