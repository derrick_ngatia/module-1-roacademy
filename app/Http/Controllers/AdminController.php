<?php

namespace App\Http\Controllers;

use App\Mail\Accepted;
use App\Mail\Rejected;
use App\Proposal;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    //
    public function index(){
        return view('admin');
    }
    public function all_proposals(){
        $proposals=Proposal::all();
        return view('admin.all_proposal',compact('proposals'));
    }
    public function preview($id){
        $proposal=Proposal::where('id',$id)->first();
        return view('admin.preview',compact('proposal'));
    }
    public function stage1(){
        $proposals=Proposal::where('stage',1)->get();
        return view('admin.stage1',compact('proposals'));
    }
    public function stage2(){
        $proposals=Proposal::where('stage',2)->get();
        return view('admin.stage1',compact('proposals'));
    }
    public function stage3(){
        $proposals=Proposal::where('stage',3)->get();
        return view('admin.stage1',compact('proposals'));
    }
    public function accept($id)
    {
        $proposal=Proposal::where('id',$id)->first();

        $user=User::where('id',$proposal->user_id)->first();
     if($proposal->stage=0){
         $proposal->update([
            'stage'=>1
         ]);
         flash('proposal promoted to stage 1')->success();
         return redirect()->back();
     }
     else if($proposal->stage=1){
         $proposal->update([
             'stage'=>2
         ]);
         Mail::to($proposal->email)->send(new Accepted($user));
         flash('proposal promoted to stage 2, and accepted and approved')->success();
         return redirect()->back();
     }
     else if($proposal->stage=2){
         flash('proposal has already been approved')->success();
         return redirect()->back();
     }

    }
    public function reject($id){
        $proposal=Proposal::where('id',$id)->first();

        $user=User::where('id',$proposal->user_id)->first();
        Mail::to($proposal->email)->send(new Rejected($user));
        $proposal->update([
            'stage'=>4
        ]);
        flash('User has been emailed')->error();
        return redirect()->back();
    }
    public function printit($id){
        $proposal=Proposal::where('id',$id)->first();
        $pdf = PDF::loadView('admin.print', compact('proposal'));
        return $pdf->download($id+'.pdf');

    }
    public function fetchUnread(){
        $proposal=Proposal::where('stage',1)->get();
        return count($proposal);
    }
}
