<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    //
    protected $fillable=['is_Submit','stage','user_id','title','organisation_name','address','phone','email','submitted_by','summary','background','activities','budget'];
}
