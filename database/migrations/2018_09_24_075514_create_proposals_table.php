<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proposals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('organisation_name');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->string('submitted_by');
            $table->string('submitted_title');
            $table->text('summary');
            $table->text('background');
            $table->text('activities');
            $table->integer('budget');
            $table->boolean('is_Submit')->default(false);
            $table->smallInteger('stage')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proposals');
    }
}
